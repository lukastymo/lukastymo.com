import org.scalafmt.sbt.ScalafmtPlugin.autoImport.scalafmtOnCompile
import sbt.Keys.{
  organization,
  parallelExecution,
  resolvers,
  scalaVersion,
  scalacOptions,
  semanticdbEnabled,
  semanticdbVersion
}
import sbt._
import scalafix.sbt.ScalafixPlugin.autoImport.{ scalafixDependencies, scalafixScalaBinaryVersion, scalafixSemanticdb }

object dependencies {
  val scalaVer         = "2.13.6"
  val scalaTestVersion = "3.2.10"

  ThisBuild / semanticdbEnabled := true
  ThisBuild / semanticdbVersion := scalafixSemanticdb.revision
  ThisBuild / scalafixDependencies += "com.github.liancheng" %% "organize-imports" % "0.5.0"
  ThisBuild / scalaVersion := scalaVer
  ThisBuild / scalafixScalaBinaryVersion := "2.13"

  lazy val compilerOptions = Seq(
    "-unchecked",
    "-deprecation",
    "-encoding",
    "utf8",
    "-feature",
    "-language:postfixOps",
    "-language:implicitConversions",
    "Wunused:imports",
    "-Xfatal-warnings"
  )
  lazy val commonDeps = Seq(
    "org.scalatest" %% "scalatest" % scalaTestVersion % "test"
  )

  lazy val commonSettings = Seq(
    organization := "com.lukastymo",
    scalaVersion := scalaVer,
    resolvers += "Typesafe Releases" at "https://repo.typesafe.com/typesafe/releases/",
    Test / parallelExecution := false,
    scalacOptions ++= compilerOptions,
    scalafmtOnCompile := true
  )

}
