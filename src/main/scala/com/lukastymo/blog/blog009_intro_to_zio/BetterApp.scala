package com.lukastymo.blog.blog009_intro_to_zio

import zio._

import java.io.IOException

object BetterApp extends ZIOAppDefault {
  def run: ZIO[Any with ZIOAppArgs with Scope, Any, Any] = {
    val program: ZIO[Any, IOException, ExitCode] = for {
      _    <- Console.printLine("What's your name")
      name <- Console.readLine
      _    <- Console.printLine(s"Hi $name")
    } yield ExitCode.success

    program
  }
}
