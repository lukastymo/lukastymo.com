import dependencies._

lazy val versions = new {
  val zio = "2.0.0-RC5"
}

lazy val zioDeps = Seq(
  "dev.zio" %% "zio"          % versions.zio,
  "dev.zio" %% "zio-test"     % versions.zio,
  "dev.zio" %% "zio-test-sbt" % versions.zio
)

lazy val otherDeps = Seq(
  "com.beachape" %% "enumeratum" % "1.7.0"
)

lazy val lukastymo = (project in file("."))
  .settings(commonSettings)
  .settings(
    name := "lukastymo-com",
    libraryDependencies ++= commonDeps ++ zioDeps ++ otherDeps
  )
