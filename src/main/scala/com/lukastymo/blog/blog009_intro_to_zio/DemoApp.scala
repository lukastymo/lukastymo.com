package com.lukastymo.blog.blog009_intro_to_zio

import zio._

import java.io.IOException
import scala.concurrent.Future
import scala.io.StdIn
import scala.util.Try

object DemoApp extends App {

  val x: IO[IOException, String]       = Console.readLine
  val y: ZIO[Any, IOException, String] = Console.readLine
  println(x)

  // Console - a module

  val myApp =
    for {
      _    <- Console.printLine("What's your name?")
      name <- Console.readLine
      _    <- Console.printLine(s"Hello $name")
    } yield ()

  // two ways to run it
  // extends ZIOAppDefault or
  val runtime: Runtime[Any] = Runtime.default
  runtime.unsafeRun(myApp)

  //
  // Procedural vs Functional
  //

  // Partial, do not return value for some input e.g. throw exception for y=0
  def div(x: Double, y: Double): Double = ???

  // Non-deterministic - different output for the same input
  def greet(name: String): String =
    name + scala.util.Random.nextString(10)

  // Impure, side-effects (mutate or interact with external world
  def say(thing: String): Unit = println(thing)

  // Functional:
  // Total
  def div2(x: Double, y: Double): Option[Double] = ???

  // Deterministic - same output for the same input
  def greet2(name: String, seed: Long): (String, Long) = {
    val newSeed = new scala.util.Random(seed).nextInt()
    val value   = new scala.util.Random(seed).nextString(10) + name
    (value, newSeed)
  }

  val (value1, nextSeed) = greet2("Lukasz", 42L)
  val (value2, _)        = greet2("Alice", nextSeed)
  println(s"$value1 $value2")

  // Pure
  def say2(thing: String): Task[Unit] = Console.printLine(thing)

  // Interaction with external world require partiality, non-determinism and side-effects
  // FP models interactions with the real world

  //
  // Effects
  //
  val r0: ZIO[Any, Nothing, Int]    = ZIO.succeed(42)
  val r1: ZIO[Any, String, Nothing] = ZIO.fail("no way")
  Some(2) // Option is an effect

  // Important (!) how to convert one effect into another. Why?
  // because for-comprehension requires only one type of effect

  val r2: ZIO[Any, Option[Nothing], Int] = ZIO.fromOption(Some(2)) // strange error, we can map it
  val r3: ZIO[Any, String, Int]          = ZIO.fromOption(Some(2)).mapError(_ => "cannot get value from None")

  // ZIO[Any, A, B] === IO[A, B]
  val r4: IO[IllegalArgumentException, Int] =
    ZIO.fromOption(Some(42)).mapError(_ => new IllegalArgumentException("no value"))

  val r5: IO[String, Nothing] = ZIO.fromEither(Left("no value"))
  // IO[Throwable, A] === Task[A]
  val r6: Task[Int] = ZIO.fromTry(Try(42 / 0))

  // Important (!) Future are not very functional programming friendly (violating referential transparency by being eagerly evaluated.
  // Here is the way to fix them by converting to ZIO effect
  val fut: Future[Int] = Future.successful(42)
  // Future.failed(exception: Throwable) so the result of fromFuture must be Task (Throwable in error side)
  val r7: Task[Int] = ZIO.fromFuture(implicit ec => fut)

  //
  // Converting Side Effects
  // Side effects (Synchronous (request -> response)
  //

  val r8: Task[String] = ZIO.attempt(StdIn.readLine())
  // readLine can throw an exception therefore it's a Task. What about println, it cannot throw an exception?
  val r9: UIO[Unit] = ZIO.succeed(println(42)) // UIO[A] = ZIO[Any, Nothing, A]
  // what if the side effect throw an exception even though we used succeed?
  val r10: UIO[Int] = ZIO.succeed(42 / 0)
  //  runtime.unsafeRun(r10)

  def throwMe(x: Int): Unit =
    x match {
      case 1 => throw new IOException("some io exception")
      case _ => throw new Exception("some exception")
    }

  val r11: IO[IOException, Unit] = ZIO.attempt(throwMe(0)).refineToOrDie[IOException]
//  runtime.unsafeRun(r11)

  //
  // zipping
  //
  val r12: UIO[(String, String)]   = ZIO.succeed("4").zip(ZIO.succeed("2"))
  val r13: IO[IOException, String] = Console.printLine("aaa").zipRight(Console.readLine)
  val r14: IO[IOException, String] = Console.printLine("bbb") *> Console.readLine

  //
  // Handling Errors
  // https://zio.dev/next/overview/overview_handling_errors
  val r15 = ZIO.fail(new Exception("some error")).orElse(ZIO.succeed(42))
  println(runtime.unsafeRun(r15))

  def openFile(name: String) = {
    for {
      _ <- Console.printLine("Opening a file")
      v <- Random.nextInt.flatMap { n =>
             if (n % 5 == 10) ZIO.succeed(42)
             else ZIO.fail("cannot open the file")
           }
    } yield v
  }

  val r16 = openFile("myfile").retry(Schedule.recurs(10))
  runtime.unsafeRun(r16)
}
