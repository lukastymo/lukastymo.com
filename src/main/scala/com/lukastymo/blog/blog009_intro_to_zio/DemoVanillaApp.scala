package com.lukastymo.blog.blog009_intro_to_zio

import scala.io.StdIn

object DemoVanillaApp extends App {
  println("What's your name?")
  val name = StdIn.readLine()
  println(s"Hi $name")
}
